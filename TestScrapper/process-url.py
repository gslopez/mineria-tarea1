from bs4 import BeautifulSoup
import pandas as pd
import urllib2
import math
import re
import time

a=pd.read_csv('./csv-deptos/DEPTO-LAS-CONDES-page_urls-page-57.csv')

page_urls=a.ix

lands=[]
length=page_urls[:,0].count()
try:
	for p in range(1,length):
		#Scrapping a page
		time.sleep(1) 
		page=page_urls[p,0]
		try:
			request=urllib2.Request(page)
			html_code=urllib2.urlopen(request)
			soup=BeautifulSoup(html_code,'html.parser')
		except Exception, e:
			if '404' in str(e):

				print str(p)+' - URL no encontrada'
				continue
			else:
				raise e
		#soup = BeautifulSoup(html_code.read(),from_encoding= 'utf-8')
		#Price in CLP
		#Price in UF

		price_uf=''
		price_clp=''
		publish_date=''
		program = ''
		bedrooms= ''
		bathrooms= ''
		area=''
		area_useful=''
		area_total=''
		latitude=''
		longitude=''
		code=''

		if soup.find('p', attrs={'class':'price-ref'}):
			price_uf = soup.find('p', attrs={'class':'price-ref'}).contents[0]
			price_uf=str(price_uf).replace('.','')
			price_uf=str(price_uf).replace('UF','').strip()
			price_uf=price_uf.replace(',','.')
			price_uf=float(price_uf)
		
		if soup.find('p', attrs={'class':'price'}):
			price_clp = soup.find('p', attrs={'class':'price'}).contents[0]
			price_clp=str(price_clp).replace('.','')
			price_clp=str(price_clp).replace('$','').strip()
			price_clp=int(price_clp)

		#Code and publish date
		try:
			code = soup.find_all('p', attrs={'class':'operation-internal-code'})[0].find('strong').contents[0]
			code=code.encode(encoding='UTF-8')
			code=code.replace('C\xc3\xb3digo: ','').strip()
		except:
			print 'No se encontro code'

		try:
			publish_date = soup.find_all('p', attrs={'class':'operation-internal-code'})[1].find('strong').contents[0]
			publish_date=str(publish_date.replace('Publicada: ','').strip())
		except:
			print 'No se encontro publish_date'

		#Bathrooms and Bedrooms
		if soup.find('div', attrs={'class':'data-sheet-column data-sheet-column-programm'}):
			program = soup.find('div', attrs={'class':'data-sheet-column data-sheet-column-programm'})
			program = str(program.find('p'))
			try:
				bedrooms = re.findall('\d+',program)[0]
				bathrooms = re.findall('\d+',program)[1]
			except:
				print 'error al obtener bedrooms y bathrooms'

		#Useful and Total Area
		if soup.find('div', attrs={'class':'data-sheet-column data-sheet-column-area'}):
			area = soup.find('div', attrs={'class':'data-sheet-column data-sheet-column-area'})
			area = str(area.find('p'))
			area = re.findall('\d+',area)
			if len(area) ==2:
				area_useful = area[0]
				area_total = area[1]
			else:
				area_useful = area[0]
				area_total = area[0]


		#Latitude and longitude
		if soup.find('meta', attrs={"itemprop":"latitude"}):
			latitude = str(soup.find('meta', attrs={"itemprop":"latitude"})['content'])[0:9]
		if soup.find('meta', attrs={"itemprop":"longitude"}):
			longitude = str(soup.find('meta', attrs={"itemprop":"longitude"})['content'])[0:9]
		#Brand
		if soup.find('p', attrs={"class":"operation-contact-name" ,"itemprop":"brand"}):
			#brand = str(soup.find('p', attrs={"class":"operation-contact-name" ,"itemprop":"brand"}).contents[0])
			brand = "Inmobiliaria"
		else:
			brand ='Particular'
		lands.append([code,publish_date,brand,price_clp,price_uf,bedrooms,bathrooms,area_useful,area_total,latitude,longitude,page_urls[p,1],page_urls[p,2],page])
		print p
except Exception, e:
	print str(e)
finally:
	header = ["Codigo", "Fecha_Publicacion", "Vende", "PrecioCLP","PrecioUF","Dormitorios","banos","area_util","area_total","latitud","longitud","tipo","comuna","url"]
	pd.DataFrame(lands,columns=header).to_csv("lands.csv", index=False) #pandas	
	print 'Copiando'



