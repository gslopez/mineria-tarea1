import itertools

class Itemset:


   #items_debe ser una lista de items
   #transactions debe ser una lista de listas de strings que contienen las transacciones que incluyen a los items
   def __init__(self,items_,transactions_=None):
      self.items=set(items_)        #Guarda los items del itemset
      self.items=sorted(self.items) #Ordena los items segun el abecedario
      self.transactions=transactions_  #Guarda que transacciones contienen a este itemset
   
   #Verifica que el itemset es joinable con el otro
   def is_joinable(self,itemset):
      if len(self.items)!=len(itemset.items):
         return False

      for i in range(0,len(itemset.items)-1):
         if self.items[i]!=itemset.items[i]:
            return False

      return True
   
   def count_transactions(self):
      if self.transactions != None:
         return len(self.transactions)
      else:
         raise 'Error, transactions no definido'



   def __eq__(self, other):
      return self.items==other.items

   def __ne__(self, other):
      return self.items!=other.items

   def __gt__(self, other):
      if len(self.items)==len(other.items):   
         return self.items>other.items
      else:
         return len(self.items)>len(other.items)

   def __ge__(self, other):
      if len(self.items)==len(other.items): 
         return self.items>=other.items
      else:
         return len(self.items)>=len(other.items)

   def __le__(self, other):
      if len(self.items)==len(other.items): 
         return self.items<=other.items
      else:
         return len(self.items)<=len(other.items)

   def __lt__(self, other):
      if len(self.items)==len(other.items): 
         return self.items<other.items
      else:
         return len(self.items)<len(other.items)

   def __repr__(self):
      return 'Itemset('+str(self.items)+')T='+str(len(self.transactions))

   def __hash__(self):
      return hash(str(self.items))


   #Obtiene todas las posibles sub-combinaciones de itemsets. Incluye los casos TODOS, y de a uno
   def get_sub_itemsets(self):
      ret = []
      for i in range(0,len(self.items)):
         sub_i=list(itertools.combinations(self.items, i+1))
         ret=ret+sub_i
      # print ret
      return ret

   # Devuelve todos los items del itemset que estan en el itemset, pero no en el parametro items
   def get_complementary_items(self,items):
      ret = []
      for i in self.items:
         if i not in items:
            ret.append(i)
      return ret
   #Realiza un join entre el itemset1 y el 2 (Agregar el ultimo elemento de itemset2 a una copia de itemset1)
   #Se debe hacer solo si is_joinable==true
   def join(itemset1,itemset2):
      joined_items=itemset1.items[:]
      joined_items.append(itemset2.items[-1])
      transactions=Itemset.find_transactions(itemset1,itemset2,joined_items)
      ret = Itemset(joined_items,transactions)
      return ret

   #Revisa las transacciones de itemset1 e itemset2, y selecciona las que contienen a joined_items
   #Asume que joined_items son los items que resultan de hacer el join de itemset1 e itemset2, por lo que revisa las transacciones
   #del que tenga menos transacciones de los dos
   def find_transactions(itemset1,itemset2,joined_items):
      ret=[]
      items1=itemset1.items
      items2=itemset2.items
      itemset_selected=None
      if len(items1)<len(items2):
         itemset_selected=itemset1
      else:
         itemset_selected=itemset2

      transactions=itemset_selected.transactions
      for transaction in transactions:
         is_valid=True
         for ji in joined_items:
            if ji not in transaction:
               is_valid=False
               break
         if is_valid:
            ret.append(transaction)
      return ret


