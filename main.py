
from itemset_processor import ItemsetProcessor
from frequently_itemset_manager import FrequentlyItemsetManager
from itemset import Itemset

transactions_file = open('transactions3.csv', 'r') # El archivo transactions.txt debe contener las transacciones
min_support=8
association_rules=open('association_rules.txt','wb')

transactions_lines = transactions_file.readlines()

transactions=[]
for i in range(0,len(transactions_lines)):
	transaction_line=transactions_lines[i].strip().lower()#quito espacios y enter, y paso todo a minuscula
	transaction=transaction_line.split(',')
	transactions.append(transaction)


processor=ItemsetProcessor(min_support)#Esta clase tiene metodos para manejar los itemsets
manager=FrequentlyItemsetManager(len(transactions_lines))# Aqui se guardan los itemsets frecuentes


itemsets=processor.get_first_itemsets(transactions)#obtiene los itemsets desde las transacciones (ojo, tambien reconoce en que transacciones sale el item)
itemsets=processor.get_valid_itemsets(itemsets)#deja solo los itemsets que cumplan con el soporte minimo
manager.add_itemsets(itemsets)# guardo los itemsets frecuentes

while len(itemsets)>0:
	itemsets=processor.get_join_itemset(itemsets)
	itemsets=processor.get_valid_itemsets(itemsets)
	manager.add_itemsets(itemsets)


#PARTE 2:
#AHORA CALCULAMOS LAS COSAS 
#Las reglas son A->B. A sera llamado left_items, B right_items, y A union B simplemente items
f_itemsets=sorted(manager.get_frequently_itemsets())
for fi in f_itemsets:
	sub_itemsets=fi.get_sub_itemsets()
	# print sub_itemsets
	items=fi.items
	itemset=fi
	support=manager.get_support(itemset)
	if len(sub_itemsets)>0:
		show_output='Itemset frecuente: '+str(itemset.items)+', T: '+str(len(itemset.transactions))
		print(show_output)
		association_rules.write(show_output+'\n')
	for si in sub_itemsets:
		left_items=list(si)
		right_items=fi.get_complementary_items(left_items)
		if len(left_items)==0 or len(right_items)==0:
			continue

		left_itemset=manager.get_frequently_itemset(left_items)
		right_itemset=manager.get_frequently_itemset(right_items)

		confidence=manager.get_confidence(left_itemset,itemset)
		lift= manager.get_lift(left_itemset,right_itemset,itemset)
		show_output="\t"+str(left_itemset.items)+" -> "+str(right_itemset.items)+" | S:"+str(support)[0:5]+ ", C:"+str(confidence)[0:5]+", L:"+str(lift)[0:5]
		print(show_output)
		association_rules.write(show_output+'\n')

association_rules.close()
# TO DO
		# confidence=manager.get_confidence(items,left_items)
		# support=manager.get_support(...)
		# lift= manager.get_lift(...)

		#GuardarEnArchivo
