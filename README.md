Para probar la tarea, es necesario definir en el main:
	-el archivo a abrir
	-soporte min
	-el archivo donde se guardaran los resultados

Breve descripcion de las clases:


itemset.py:

Clase que representa un itemset. Es la encargada de guardar los items de un itemset y las transacciones asociadas a el.

frequently_itemset_manager.py:

Aqui se guardan los itemsets frecuentes para poder ser utilizados mas adelante.

main.py:

Es el archivo principal. en el se llaman a las demas clases para cumplir el objetivo.

itemset_processor.py:

Tiene algunos metodos para procesar los itemset