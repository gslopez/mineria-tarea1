from itemset import Itemset

class ItemsetProcessor:
	

	def __init__(self,support):
		self.support=support

	def get_join_itemset(self,itemsets):
		joined_itemsets=[]
		for i in range(0,len(itemsets)):
			for k in range (i+1,len(itemsets)):
				if itemsets[i].is_joinable(itemsets[k]):
					joined_itemset=Itemset.join(itemsets[i],itemsets[k])
					joined_itemsets.append(joined_itemset)
		return joined_itemsets

	#Devuelve solo los itemsets que cumplen con el soporte
	def get_valid_itemsets(self,itemsets):
		valid_itemsets=[]
		for i in itemsets:
			if i.count_transactions()>=self.support:
				valid_itemsets.append(i);
		return valid_itemsets

	#Lee las transacciones y saca los itemsets
	def get_first_itemsets(self,transactions):
		first_itemset=[]
		first_itemset_freq=dict()
		for t in transactions:
			for i in t:
				if i in first_itemset_freq:
					first_itemset_freq[i].append(t)
				else:
					first_itemset_freq[i]=[t]

		for key in first_itemset_freq:
			if len(first_itemset_freq[key])>=self.support:
				itemset =Itemset([key],first_itemset_freq[key])
				first_itemset.append(itemset)

		return sorted(first_itemset)
