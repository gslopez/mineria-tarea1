from itemset import Itemset

class FrequentlyItemsetManager:
	#Guarda los itemsets frecuentes segun la cantidad de items que tiene

	def __init__(self,total_transactions):
		self.frequently_itemsets=dict()
		self.total_transactions=total_transactions

	def add_itemsets(self,itemsets):

		for itemset in itemsets:
			
			self.frequently_itemsets[itemset]=itemset


		# for itemset in itemsets:
		# 	key=len(itemset.items)
		# 	if key in self.frequently_itemsets:
		# 		self.frequently_itemsets[key].append(itemset)
		# 	else:
		# 		self.frequently_itemsets[key]=[itemset]

#cuenta cuantas transacciones tienen al itemset
	def get_frequently_itemset(self,items):
		key = Itemset(items)
		if key in self.frequently_itemsets:
			return self.frequently_itemsets[key]
		return None

	def get_frequently_itemsets(self):
		freq_itemsets=list(self.frequently_itemsets.keys())
		return freq_itemsets

		# if len(itemset.items) in self.frequently_itemsets:
		# 	itemsets_list=self.frequently_itemsets[len(itemset.items)] #Saco la lista que deberia contener este itemset
		# 	for ist in itemsets_list:
		# 		if ist>itemset:
		# 			break

		# 		if ist==itemset:
		# 			return len(ist.transactions)

		# else:
		# 	return 0

	def get_confidence(self,left_itemset,itemset):
		confidence=float(len(itemset.transactions))/len(left_itemset.transactions)
		return confidence

	def get_support(self,itemset):
		support=float(len(itemset.transactions))/self.total_transactions
		return support

	#Aqui utilizar el metodo count_transactions ! ! ! !
	def get_lift(self,left_itemset,right_itemset,itemset): 
		# LIFT = Confianza/SoporteDerecha
		lift= self.get_confidence(left_itemset,itemset)/(float(len(right_itemset.transactions))/self.total_transactions)
		return lift



